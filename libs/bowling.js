var manager = 
{
	vars: 
	{
		frame_round: 0,
		1: 
		{
			result_score: [],
			frame_score: [],
			total_score: [],
			table: {}
		},
		2:
		{
			result_score: [],
			frame_score: [],
			total_score: [],
			table: {}
		},
		ui:
		{
			strike: "X",
			spare: "/"
		}
	},
	init: function()
	{
		//ref output(s) label
		//player1
		var table1_rows = $("#table1 > tbody > tr");
		manager.vars[1].table.result_score = table1_rows[1].cells;
		manager.vars[1].table.frame_score = table1_rows[2].cells;
		manager.vars[1].table.total_score = table1_rows[3].cells;
		//player2
		var table2_rows = $("#table2 > tbody > tr");
		manager.vars[2].table.result_score = table2_rows[1].cells;
		manager.vars[2].table.frame_score = table2_rows[2].cells;
		manager.vars[2].table.total_score = table2_rows[3].cells;
	},
	main: function()
	{				
		//Compute result
		manager.compute_result();
		manager.output();
		
		if(manager.vars.frame_round === 10)
			manager.disable_button();
	},
	compute_result: function()
	{
				
		manager.vars.frame_round++;
		var frame_round = manager.vars.frame_round;
		
		//compute frame per player
		for(var player=1; player <= 2; player++)
		{
			var frame_object =
			{
				strike: false,
				spare: false,
				final_score: false,
				roll: {1:undefined,2:undefined}
			}
			var sum_pins_kd = 0;
			var total_pins = 10;
			//compute number of pins knocked down per roll
			for(var roll=1; roll <= 2; roll++)
			{
				var pins_kd = manager.knock_pins(total_pins);
				sum_pins_kd += pins_kd;
				total_pins -= pins_kd;
				frame_object.roll[roll] = pins_kd; //register number of KD pins per roll
				if(pins_kd === 10 && roll === 1) //strike
				{
					frame_object.strike = true;
					if(frame_round === 10) //last frame
						frame_object.extra_shots = {num:2,1:undefined,2:undefined};
					break;
				}
				else if(sum_pins_kd === 10 && roll === 2) //spare
				{
					frame_object.spare = true;
					if(frame_round === 10) //last frame
						frame_object.extra_shots = {num:1,1:undefined};
				}
				else //normal score
					frame_object.final_score = true;
					
			}			
			manager.vars[player].result_score.push(frame_object);
			
			if(frame_round === 10 && (manager.vars[player].result_score[frame_round-1].extra_shots !== undefined)) //last frame: run bonus rolls
			{
				var total_pins = 10;
				for(var roll=1; roll <= manager.vars[player].result_score[frame_round-1].extra_shots.num; roll++)
				{
					var pins_kd = manager.knock_pins(total_pins); //one or two rolls of 10 pins
					sum_pins_kd += pins_kd;
					manager.vars[player].result_score[frame_round-1].extra_shots[roll] = pins_kd;
				}
				manager.vars[player].result_score[frame_round-1].final_score = true;
			}
			
			manager.vars[player].frame_score.push(sum_pins_kd);

			//Update frame score per player
			if(frame_round != 1)
			{
				//check for strikes or spares in last frame
				if(manager.vars[player].result_score[frame_round - 2].strike) //update frame score (sum next two scores)
				{
					manager.vars[player].frame_score[frame_round - 2] += manager.vars[player].result_score[frame_round - 1].roll[1];
					if(manager.vars[player].result_score[frame_round - 1].roll[2] !== undefined)
					{
						manager.vars[player].frame_score[frame_round - 2] += manager.vars[player].result_score[frame_round - 1].roll[2];
						manager.vars[player].result_score[frame_round - 2].final_score = true; //final score!
					}
					else if(frame_round === 10 && (manager.vars[player].result_score[frame_round-1].extra_shots !== undefined))
					{
						manager.vars[player].frame_score[frame_round - 2] += manager.vars[player].result_score[frame_round-1].extra_shots[1];
						manager.vars[player].result_score[frame_round - 2].final_score = true; //final score!
					}
				}
				else if(manager.vars[player].result_score[frame_round - 2].spare) // sum next score
				{
					manager.vars[player].frame_score[frame_round - 2] += manager.vars[player].result_score[frame_round - 1].roll[1];
					manager.vars[player].result_score[frame_round - 2].final_score = true; //final score!
				}
				
				//check for strikes in last 2 frames (if exists)
				try
				{
					if(manager.vars[player].result_score[frame_round - 3].strike
					&& !manager.vars[player].result_score[frame_round - 3].final_score)
					{
						manager.vars[player].frame_score[frame_round - 3] += manager.vars[player].result_score[frame_round - 1].roll[1];
						manager.vars[player].result_score[frame_round - 3].final_score = true; //final score!
					}
				}
				catch(err)
				{} //do nothing
			}
			
			//update total score
			manager.vars[player].total_score = []; //reset
			var sum_frame_score = 0;
			for(var i=0; i < frame_round; i++)
			{
				sum_frame_score += manager.vars[player].frame_score[i];
				manager.vars[player].total_score.push(sum_frame_score);
			}
		}
		
	},
	knock_pins: function(total_pins)
	{
		//Number of pinks Knocked Down per roll [0 - 10]
		var pins_kd = Math.round(Math.random() * total_pins);
		return pins_kd;
	},
	disable_button: function() //when match ends
	{
		$("#submit_button")[0].disabled = true;
		$("#submit_button")[0].value = "Match Ended!";
	},
	output: function(array)
	{	
		for(var player=1; player <= 2; player++)
		{
			//result score per player
			var result_txt = "";
			if(manager.vars[player].result_score[manager.vars.frame_round-1].strike)
				result_txt = manager.vars.ui.strike;
			else if(manager.vars[player].result_score[manager.vars.frame_round-1].spare)
				result_txt = manager.vars[player].result_score[manager.vars.frame_round-1].roll[1] + manager.vars.ui.spare;
			else
				result_txt = manager.vars[player].result_score[manager.vars.frame_round-1].roll[1] + " " +
			manager.vars[player].result_score[manager.vars.frame_round-1].roll[2];
			
			if(manager.vars[player].result_score[manager.vars.frame_round-1].extra_shots !== undefined)
				for(var i=1; i <= manager.vars[player].result_score[manager.vars.frame_round-1].extra_shots.num; i++)
					result_txt += " " + manager.vars[player].result_score[manager.vars.frame_round-1].extra_shots[i];
			
			manager.vars[player].table.result_score[manager.vars.frame_round].innerHTML = result_txt;
			
			for(var i=0; i < manager.vars[player].frame_score.length; i++)
			{
				//frame score
				manager.vars[player].table.frame_score[i+1].innerHTML = manager.vars[player].frame_score[i];
				//total score
				manager.vars[player].table.total_score[i+1].innerHTML = manager.vars[player].total_score[i];
			}
		}
	}
}

$( document ).ready(function() 
{
	manager.init();
});